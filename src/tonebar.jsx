import ForgeUI, { Fragment, Image } from "@forge/ui";

const DEFAULT_WIDTH = 900;
const height = 25;

const DEFAULT_TONE_SCALE = 400;
const ToneBar = ( { tone, width } ) => {
  width = width || DEFAULT_WIDTH
  const scale = width ? width / 2 : DEFAULT_TONE_SCALE
  const svg = tone ? `
  <svg
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:cc="http://creativecommons.org/ns#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns="http://www.w3.org/2000/svg"
    xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
    xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
    pretty_print="False"
    baseProfile="full"
    style="stroke-linejoin: round; stroke:rgb(223, 225, 255); fill: none;"
    version="1.1"
    viewBox="0 0 ${width} ${height}"
    width="auto"
    height="${height}px"
    id="svg2">
    <g class="bar">
      <rect width="${tone.tone_score * scale}" height="19" fill="rgb(222, 235, 255)"></rect>
      <rect width="${scale}" height="19"></rect>
      <text x="${scale + 16}" 
        y="9.5" dy=".35em" 
        style="fill: #67717e; stroke: none; font-size: 20px; text-transform: uppercase;" 
        font-weight="600" font-family="Helvetica,sans-serif">${tone.tone_name}</text>
    </g>

  </svg>
  ` : ''
  return  tone &&
      <Fragment>
            <Image
              src={`data:image/svg+xml;utf8,${encodeURIComponent(svg)}`}
              alt='header'
            />
      </Fragment>
            || 
            null
          
};

export default ToneBar
