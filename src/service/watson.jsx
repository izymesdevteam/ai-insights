import {checkResponse} from "../util/common";
import api from "@forge/api";

const WATSON_API_URL = 'api.au-syd.tone-analyzer.watson.cloud.ibm.com/instances/e8685c35-2709-480a-b66c-6a02d7ed8985/v3/tone?version=2020-06-21&sentences=false';
const WATSON_API_URL_CHAT = 'api.au-syd.tone-analyzer.watson.cloud.ibm.com/instances/e8685c35-2709-480a-b66c-6a02d7ed8985/v3/tone_chat?version=2017-09-21';

const WATSON_API_KEY = 'apikey:kbRHaup0zN9KLAlaj3g_1VFV09FohimxQnQfqDmBmaad'

/**
 * get the one for a tone for a single comment body text
 * @param  issueComment 
 */
export async function getWatsonCommentTone(issueComment) {
    //Get the API key and endpoint from forge environment vars
    //const { WATSON_API_KEY, WATSON_API_URL } = process.env

    // Evaluate the field tone using the Watson API
    const watsonResponse = await api.fetch(`https://${WATSON_API_KEY}@${WATSON_API_URL}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify({ text: issueComment } )
    });
    await checkResponse('Watson API', watsonResponse);
    console.log('Watson Single Comment Response', JSON.stringify(await watsonResponse.json()) )

    return await watsonResponse.json()
}

/**
 * get the tone(s) for the complete issue text including description and all comments
 * @param {*} issueFields 
 */
export async function getWatsonTones(issueFields) {
    //Get the API key and endpoint from forge environment vars
    //const { WATSON_API_KEY, WATSON_API_URL } = process.env
    
    const issueComments = issueFields.comment.comments.map((c) => c.body).join('\n')
    const issueText = `${issueFields.description}.\n${issueComments}` 

    // console.log("issueText", issueText)

    // Evaluate the field tone using the Watson API
    const watsonResponse = await api.fetch(`https://${WATSON_API_KEY}@${WATSON_API_URL}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify({ text: issueText } )
    });
    await checkResponse('Watson API', watsonResponse);
    // console.log('Watson Response', JSON.stringify(await watsonResponse.json()) )

    // Watson response object
    // {"document_tone":
    //     {"tones":[
    //         {
    //             "score":0.65652,
    //             "tone_id":"joy",
    //             "tone_name":"Joy"
    //         },
    //         {
    //             "score":0.598522,
    //             "tone_id":"sadness",
    //             "tone_name":"Sadness"
    //         },
    //         {
    //             "score":0.871674,
    //             "tone_id":"tentative",
    //             "tone_name":"Tentative"
    //         }
    //         ]
    //     }}

    return await watsonResponse.json()

}

export async function getWatsonTonesChat(utterances) {

    const utterancesTone = await api.fetch(`https://${WATSON_API_KEY}@${WATSON_API_URL_CHAT}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8',
        },
        body: JSON.stringify({ utterances: utterances } )
    })
    checkResponse('WatsonApi Chat', utterancesTone)
    return await utterancesTone.json()
}

