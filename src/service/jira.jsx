import api, { store } from "@forge/api";
import {getWatsonTones} from "./watson";
import { checkResponse } from '../util/common';

const customFieldPropertyKey = "toneCustomField";
const toneTimelinePropertyKey = 'ai-tone-timeline';
const toneTimelineChatPropertyKey = 'ai-tone-timeline-chat';
const tonePropertyKey = 'ai-tone';

/**
 * Gets the issue description and comment fields from Jira
 */
export async function getJiraIssueFields(issueKey) {
    // Fetch issue fields to evaluate tone from Jira
    const issueResponse = await api.asApp().requestJira(`/rest/api/2/issue/${issueKey}?fields=description,comment,reporter,assignee&expand=changelog`);
    checkResponse('jira issues', issueResponse)
    const issueResponseJson = await issueResponse.json()
    // console.log(`Retrieving issue fields for issue ${issueKey}`, issueResponseJson.fields)
    return issueResponseJson.fields
}
/**
 * Save the tone results to Jira custom fields and store api
 */
export async function saveToneFields(issueKey, toneName, toneScore) {
    // Store tone name in custom field
    await requestJira("PUT", `/rest/api/2/issue/${issueKey}/properties/${customFieldPropertyKey}`, toneName, () => {});
    console.log(`Updated the custom field of issue ${issueKey} to ${toneName}`);

    // Store tone name, score and timestamp on the Jira issue
    const toneDate = Date.now();
    await store.onJiraIssue(issueKey).set(tonePropertyKey, { tone_name: toneName, tone_score: toneScore, tone_date: toneDate });
}

/**
 * Get the overall tone name, score and date for an issue
 * @returns array
 */
export async function getCurrentTone(issueKey) {
    // Get the tone object from the Jira store api
    const toneObjectResponse = await store.onJiraIssue(issueKey).get(tonePropertyKey);
    // console.log('toneObjectResponse', toneObjectResponse)
    return toneObjectResponse
}

/**
 * Get the tone history for an issue
 * @returns array
 */
export async function getToneTimeline(issueKey) {
    // Get the tone timeline object from the Jira store api
    const toneTimelineResponse = await store.onJiraIssue(issueKey).get(toneTimelinePropertyKey);
    // console.log('toneTimelineResponse', toneTimelineResponse)
    return toneTimelineResponse
}

export async function saveToneTimeline(issueKey, toneTimeline) {
    await store.onJiraIssue(issueKey).set(toneTimelinePropertyKey, toneTimeline)
}

export async function getToneTimelineChat(issueKey) {
    // Get the tone timeline object from the Jira store api
    const response = await store.onJiraIssue(issueKey).get(toneTimelineChatPropertyKey);
    // console.log('toneTimelineChatResponse', response)
    return response
}

export async function saveToneTimelineChat(issueKey, toneTimeline) {
    await store.onJiraIssue(issueKey).set(toneTimelineChatPropertyKey, toneTimeline)
}

/**
 * Wrapper function for Jira product fetch api
 */
async function requestJira(method, url, body, responseTransformer = async r => await r.json()) {
    console.log(`REQUEST: ${method} ${url}`);
    const options = { method: method }
    if (body) {
        options.body=JSON.stringify(body)
    }
    const response = await api.asApp().requestJira(url, options);
    return responseTransformer(response);
}