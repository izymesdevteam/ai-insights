import ForgeUI, {
    CustomField,
    CustomFieldView,
    render,
    StatusLozenge,
    Text,
    useProductContext,
    useState
} from "@forge/ui";
import {getJiraIssueFields, getCurrentTone, getToneTimeline, saveToneFields, saveToneTimeline, getToneTimelineChat, saveToneTimelineChat} from "./service/jira";
import {getWatsonTones, getWatsonCommentTone, getWatsonTonesChat} from "./service/watson";
import ToneBar from "./tonebar";
import {store} from "@forge/api";

// Sort tones by score
const compare = (a, b) => {
    if (a.score > b.score) return -1;
    if (b.score > a.score) return 1;
    return 0;
}

// Save tones to custom field on issue update
export async function onIssueUpdate(event) {
    const { activityItem: { object: { localResourceId: issueKey }, eventType } } = event;
    console.log('on issue update', issueKey, eventType )

    // Fetch issue fields to evaluate from Jira
    const issueFields = await getJiraIssueFields(issueKey);

    // Get all the comments for this issue
    const issueComments = issueFields.comment.comments;

    // const toneTimeline = await getCommentsToneTimeline(issueKey, issueComments);
    // saveToneTimeline(issueKey, toneTimeline);

    const toneTimelineChat = await getIssueToneTimelineChat(issueKey, issueFields);
    saveToneTimelineChat(issueKey, toneTimelineChat);

    // get the issue tones from Watson
    const issueTones = await getWatsonTones(issueFields);

    issueTones.document_tone.tones.sort(compare);

    if (issueTones.document_tone.tones.length) {
        const toneName = issueTones.document_tone.tones[0].tone_name;
        const toneScore = issueTones.document_tone.tones[0].score
        console.log(`Updating the custom field for issue ${issueKey}`, toneName, toneScore);
        await saveToneFields(issueKey, toneName, toneScore);
    }
}

const View = () => {
    const { platformContext: { fieldValue, issueKey } } = useProductContext();
    const [tone] = useState(async () => await getCurrentTone(issueKey));

    return (
        <CustomFieldView>
            <Text>
                Issue mood: <StatusLozenge text={`${fieldValue || 'n/a'}`} appearance="inprogress" /> {Math.round(tone.tone_score * 100)}%
            </Text>
            <ToneBar tone={tone} width={400} />
        </CustomFieldView>
    );
};


export const run = render(
    <CustomField
        view={<View/>}
    />
);

/*
{"utterances_tone":[{"utterance_id":0,"utterance_text":"Hello, I'm having a problem with your product.","tones":[{"score":0.686361,"tone_id":"polite","tone_name":"Polite"}]},{"utterance_id":1,"utterance_text":"OK, let me know what's going on, please.","tones":[{"score":0.92724,"tone_id":"polite","tone_name":"Polite"}]},{"utterance_id":2,"utterance_text":"Well, nothing is working :(","tones":[{"score":0.997795,"tone_id":"sad","tone_name":"Sad"}]},{"utterance_id":3,"utterance_text":"Sorry to hear that.","tones":[{"score":0.730982,"tone_id":"polite","tone_name":"Polite"},{"score":0.672499,"tone_id":"sympathetic","tone_name":"Sympathetic"}]}]}
*/
async function getIssueToneTimelineChat(issueKey, issueFields) {
    const reporterName = issueFields.reporter.displayName // customer
    const commentUtterancesWithMetadata = issueFields.comment.comments.map((issueComment) => {
        return {
            text: issueComment.body,
            user: issueComment.author.displayName !== reporterName ? 'agent' : 'user',
            date: issueComment.created,
            cId: issueComment.id
        }
    })
        
    // add the issue description (by reporter) at the beginning
    const utterancesWithMetadata = [{
            cId: 'summary',
            text: issueFields.description,
            user: 'user'
        }].concat(commentUtterancesWithMetadata)

    const storedTimelineChat = await getToneTimelineChat(issueKey, issueFields)

    // console.log('stored timeline chat', {storedTimelineChat}, {utterancesWithMetadata})
    const newUtterencesWithMetadata = utterancesWithMetadata.filter((ut) => !storedTimelineChat.some(tc => ut.cId === tc.comment_id ))
    if (!newUtterencesWithMetadata.length) {
        console.log('no new utterances found, returning stored timelineChat')
        return storedTimelineChat;
    }

    // console.log('analysing utterances', utterancesWithMetadata)
    const toneUtterancesTimelineChat = await getWatsonTonesChat(utterancesWithMetadata.map(um => { 
        const {date, cId, ...utterance} = um;
        return utterance
        }))
    const toneTimeline = [] 
    toneUtterancesTimelineChat.utterances_tone.forEach((ut) => {
        if (ut.tones.length) {
            ut.tones.sort(compare)
            // console.log('utterance tone', ut.utterance_text, ut.tones[0])
            toneTimeline.push({
                comment_id: utterancesWithMetadata[ut.utterance_id].cId,
                comment_user: utterancesWithMetadata[ut.utterance_id].user,
                comment_date: utterancesWithMetadata[ut.utterance_id].date || '',
                tone: {
                    name: ut.tones[0].tone_name,
                    score: ut.tones[0].score
                }
            })
        }
    })
    console.log('finished processing tone timeline chat', toneTimeline)
    return toneTimeline
}

async function getCommentsToneTimeline(issueKey, issueComments) {
    const toneTimeline = await getToneTimeline(issueKey) || [];

    let newIssueComments = issueComments;
    if (toneTimeline) {
        // console.log('Tone Timeline retrieved', toneTimeline);
        newIssueComments = issueComments.filter((iC) => !toneTimeline.some((tT) => iC.id === tT.comment_id));
        // console.log('New Issue Comments', newIssueComments);
    }
    // Use Promise.all here
    await Promise.all(newIssueComments.map(async (comment) => {
        console.log('checking tone for comment id', comment.id)
        const tone = await getWatsonCommentTone(comment.body);
        if (tone.document_tone.tones.length) {
            tone.document_tone.tones.sort(compare);
            const newToneName = tone.document_tone.tones[0].tone_name;
            const newToneScore = tone.document_tone.tones[0].score;
            console.log(`Comment ${comment.id} Tone Name/Score`, newToneName, newToneScore);
            toneTimeline.push({
                comment_id: comment.id,
                comment_date: comment.created,
                tone: {
                    name: newToneName,
                    score: newToneScore
                }
            });
        }

    }));

    // console.log('Finished processing issue comments', toneTimeline);
    return toneTimeline;
}
//[
//     array = {
//         self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/issue/10009/comment/10005',
//         id: '10005',
//         author: {
//             self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/user?accountId=5e7884bf97217f0c3437dbb0',
//             accountId: '5e7884bf97217f0c3437dbb0',
//             emailAddress: 'michael@moriarty.com.au',
//             avatarUrls: [Object],
//             displayName: 'Michael Moriarty',
//             active: true,
//             timeZone: 'Australia/Sydney',
//             accountType: 'atlassian'
//         },
//         body: 'And furthermore…. just not happy',
//         updateAuthor: {
//             self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/user?accountId=5e7884bf97217f0c3437dbb0',
//             accountId: '5e7884bf97217f0c3437dbb0',
//             emailAddress: 'michael@moriarty.com.au',
//             avatarUrls: [Object],
//             displayName: 'Michael Moriarty',
//             active: true,
//             timeZone: 'Australia/Sydney',
//             accountType: 'atlassian'
//         },
//         created: '2020-07-09T15:10:41.464+1000',
//         updated: '2020-07-09T15:10:41.464+1000',
//         jsdPublic: true
//     },
//     {
//         self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/issue/10009/comment/10006',
//         id: '10006',
//         author: {
//             self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/user?accountId=5e7884bf97217f0c3437dbb0',
//             accountId: '5e7884bf97217f0c3437dbb0',
//             emailAddress: 'michael@moriarty.com.au',
//             avatarUrls: [Object],
//             displayName: 'Michael Moriarty',
//             active: true,
//             timeZone: 'Australia/Sydney',
//             accountType: 'atlassian'
//         },
//         body: 'and this is comment number 2',
//         updateAuthor: {
//             self: 'https://api.atlassian.com/ex/jira/7fb8a225-8efb-4f2f-9a15-fceb7a140da3/rest/api/2/user?accountId=5e7884bf97217f0c3437dbb0',
//             accountId: '5e7884bf97217f0c3437dbb0',
//             emailAddress: 'michael@moriarty.com.au',
//             avatarUrls: [Object],
//             displayName: 'Michael Moriarty',
//             active: true,
//             timeZone: 'Australia/Sydney',
//             accountType: 'atlassian'
//         },
//         created: '2020-07-09T16:00:45.022+1000',
//         updated: '2020-07-09T16:00:45.022+1000',
//         jsdPublic: true
//     }
// ]