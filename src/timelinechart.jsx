import ForgeUI, { Fragment, Image, useProductContext, useState } from "@forge/ui";
import { getCurrentTone, getToneTimeline } from "./service/jira";
import api from "@forge/api";
import { checkResponse } from "./util/common";

// Tone chart display settings
const toneFactorMap = {
  Excited: { colour: "rgba(63, 151, 52, 0.5)", factor: 100 },
  Joy: { colour: "rgba(63, 151, 52, 0.5)", factor: 100 },
  Satisfied: { colour: "rgba(168, 240, 96, 0.5)", factor: 85 },
  Analytical: { colour: "rgba(157, 188, 92, 0.5)", factor: 75 },
  Confident: { colour: "rgba(157, 188, 92, 0.5)", factor: 75 },
  Sympathetic: { colour: "rgba(201, 240, 96, 0.5)", factor: 60 },
  Polite: { colour: "rgba(200, 223, 52, 0.5)", factor: 45 },
  Tentative: { colour: "rgba(200, 223, 52, 0.5)", factor: 30 },
  Sad: { colour: "rgba(223, 154, 52, 0.5)", factor: -30 },
  Frustrated: { colour: "rgba(223, 52, 57, 0.5)", factor: -75 },
  Angry: { colour: "rgba(223, 52, 57, 0.5)", factor: -100 },
};

const TimelineChart = ({ toneHistory, title }) => {
  // Get the issue key
  const {
    platformContext: { issueKey },
  } = useProductContext();

  const dataArray = [];
  const bgColorArray = [];
  toneHistory.forEach((comment) => {
    const toneName = comment.tone.name;
    const toneScore = (comment.tone.score * toneFactorMap[toneName]["factor"]).toFixed(2);
    const bgColour = toneFactorMap[toneName]["colour"];
    dataArray.push(toneScore);
    bgColorArray.push(bgColour);
  });

  //   console.log("Dataset for Chart", datasets);
  const chart = {
    type: "bar",
    data: {
      labels: toneHistory.map((ct) => ct.tone.name),
      datasets: [
        {
          data: dataArray,
          backgroundColor: bgColorArray,
        },
      ],
    },
    options: {
      responsive: true,
      legend: {
        display: false,
      },
      title: {
        display: true,
        text: title || "Issue satisfaction",
      },
    },
  };

  const [chartResponse] = useState(async () => {
    const response = await api.fetch("https://quickchart.io/chart/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({ chart }),
    });
    // console.log('fetch chart data', await response.json())
    await checkResponse("Chart API", response);
    return await response.json();
  });

  //   console.log("chart url", chartResponse.url);
  return (
    <Fragment>
      <Image
        src={chartResponse.url}
        alt="progress"
      />
    </Fragment>
  );
};

    
export default TimelineChart;
