import ForgeUI, { Fragment, Image, render, Text, StatusLozenge, useState, useProductContext } from "@forge/ui";
import { getJiraIssueFields, getCurrentTone, getToneTimeline, getToneTimelineChat } from "./service/jira";
import { getWatsonTones } from "./service/watson";
import ToneBar from "./tonebar";
import TimelineChart from "./timelinechart";

const TonePanel = () => {
  // Get the issue key
  const {
    platformContext: { issueKey },
  } = useProductContext();

  const [tone] = useState(async () => await getCurrentTone(issueKey));
  // const [toneHistory] = useState(async () => await getToneTimeline(issueKey));
  const [toneHistoryChat] = useState(async () => await getToneTimelineChat(issueKey));

  console.log("Rendering Panel", tone);

  return (
    <Fragment>
      <Text>
        Current mood of this issue: {(tone && <StatusLozenge text={tone.tone_name} appearance="inprogress" />) || <StatusLozenge text="Loading..." appearance="inprogress" />}{" "}
        {Math.round(tone.tone_score * 100)}%
      </Text>
      <ToneBar tone={tone} width={900} />
      {/* {toneHistory && <TimelineChart toneHistory={toneHistory} />} */}
      {toneHistoryChat && <TimelineChart toneHistory={toneHistoryChat} />}
      {toneHistoryChat && <TimelineChart {...{
        toneHistory: toneHistoryChat.filter((commentTone) => commentTone.comment_user && commentTone.comment_user === 'agent'),
        title: 'Agent scores'
        }} />}
      {toneHistoryChat && <TimelineChart {...{
        toneHistory: toneHistoryChat.filter((commentTone) => commentTone.comment_user && commentTone.comment_user === 'user'),
        title: 'User satisfaction scores'
        }} />}
    </Fragment>
  );
};
export const run = render(<TonePanel />);
